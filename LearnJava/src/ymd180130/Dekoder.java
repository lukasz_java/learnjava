package ymd180130;

public class Dekoder {
	private String produkt;
	private int ilosc;
	private String jednostka;
	
	public Dekoder(){
		
	}
	
	public void odczytDekoder(String odczyt){
		String[] kod=odczyt.split(" ");
		this.produkt=kod[0];
		this.ilosc=Integer.parseInt(kod[1]);
		this.jednostka=kod[2];
	}

	public String getProdukt() {
		return produkt;
	}

	public void setProdukt(String produkt) {
		this.produkt = produkt;
	}

	public int getIlosc() {
		return ilosc;
	}

	public void setIlosc(int ilosc) {
		this.ilosc = ilosc;
	}

	public String getJednostka() {
		return jednostka;
	}

	public void setJednostka(String jednostka) {
		this.jednostka = jednostka;
	}
	
	public String getInfo(){
		return getProdukt()+" "+getIlosc()+" "+getJednostka();
	}
	
}