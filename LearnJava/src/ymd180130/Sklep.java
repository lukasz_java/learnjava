package ymd180130;

import java.util.Scanner;

public class Sklep {
	
	
	static int n=3; // ilosc miejsca na produkty w magazynie
	//static int a=0; // aktualna liczba produktow w magazynie
	static Produkt[] magazyn=new Produkt[n];
	final static String ASORTYMENT="asortyment";
	final static String DOSTAWA="dostawa";
	final static String KUPNO="kupno";
	final static String END="end";
	static Dekoder dekoder=new Dekoder();
	
	public static void informacja(){
		System.out.println("------------------------------------------------------");
		System.out.println("W sklepie masz do wyboru 3 opcje:"
				+ "\n"+ ASORTYMENT+ "- wyswietla zawartosc magazynu "
				+ "\n" + DOSTAWA + "- dodaje produkty do magazynu "
				+ "\n" + KUPNO + "- mozesz cos kupic ze sklepu"
				+ "\n" + END +"- wyjscie z progamu");
		System.out.println("------------------------------------------------------");
	}
	
	public static void asortyment(){
		System.out.println("zawartosc magazynu:");
		for (int i=0; i<magazyn.length; i++){
			if (magazyn[i]!=null) magazyn[i].infoProdukt();
		}
	}
	
	public static void dostawa(Scanner sc){
		System.out.println("Wpisz produkt ktory chcesz dodac do magazynu w formacie:"
				+ "\nnazwa ilosc jednostka"
				+ "\nnp.: ziemniaki 20 kg.");
		dekoder.odczytDekoder(sc.nextLine());
		System.out.println(dekoder.getProdukt());
		
		for (int i=0; i<magazyn.length; i++){
			if (magazyn[i]!=null && magazyn[i].getNazwa().equals(dekoder.getProdukt())){
				magazyn[i].dostawa(dekoder.getIlosc());
				break;
			}
			else if (magazyn[i]==null) {

				magazyn[i]=new Produkt();
				magazyn[i].setNazwa(dekoder.getProdukt());
				magazyn[i].setIlosc(dekoder.getIlosc());
				magazyn[i].setJednostka(dekoder.getJednostka());
				
				break;
			}
			else if (i==(magazyn.length-1)) System.out.println("W magazynie nie ma miejsca na dostawe");
		}
	}
	
	public static void kupno(Scanner sc){
		boolean czyJest=false;
		int i;
		System.out.println("Wpisz co chcesz kupic w formacie:"
				+ "\nnazwa ilosc jednostka"
				+ "\nnp.: ziemniaki 20 kg.");
		dekoder.odczytDekoder(sc.nextLine());
		//String coKupic=sc.nextLine();
		for (i=0; i<magazyn.length; i++){
			if (magazyn[i]!=null && magazyn[i].getNazwa().equals(dekoder.getProdukt()) && magazyn[i].getIlosc()>=dekoder.getIlosc()){
				czyJest=true;
				magazyn[i].kupno(dekoder.getIlosc());
				if (magazyn[i].getIlosc()==0){
					for (int j=i; j<magazyn.length-1; j++){
						magazyn[j]=magazyn[j+1];
					}
					magazyn[magazyn.length-1]=null;
				}
				break;
			}
		}
		//System.out.println(i);
		if (czyJest) System.out.println("Kupiles wlasnie "+dekoder.getProdukt());
		else //if (!magazyn[i].getNazwa().equals(dekoder.getProdukt())){
		{ System.out.println(dekoder.getInfo() +" nie ma w magazynie");
		}

	}
	
	
	public static void opcje(Scanner sc, String input){
		//String input=sc.nextLine();
		switch (input){
			case ASORTYMENT:
				asortyment();
				break;
			case DOSTAWA:
				dostawa(sc);
				asortyment();
				break;
			case KUPNO: 
				kupno(sc);
				break;
			default:
				System.out.println("Nic nie wybrales, sprobuj jeszcze raz.");
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc=new Scanner(System.in);
		System.out.println("Witaj w sklepie!");
		while (true){
			informacja();
			String input=sc.nextLine();
			if (input.equals(END)){
				System.out.println("Zapraszamy ponownie!");
				break;
			}
			opcje(sc, input);
		}
		sc.close();
		
	}

}