package ymd180130;

import java.util.Scanner;

public class Basic58 {

	public static void main(String[] args) {
		String S, result[];

		Scanner sc = new Scanner(System.in);

		System.out.print("Input the string : ");
		S = sc.nextLine();
		sc.close();
		result = S.trim().split(" ");
		for (int i = 0; i < result.length; i++) {
			System.out.print(result[i].toUpperCase().charAt(0) + result[i].substring(1) + " ");
		}
	}
}
