package ymd180130;

public class Produkt {
	
	private String nazwa;
	private int ilosc;
	private String jednostka;
	
/*	public Produkt(String nazwa, double ilosc, String jednostka) {
		super();
		this.nazwa = nazwa;
		this.ilosc = ilosc;
		this.jednostka = jednostka;
	}*/
	
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public int getIlosc() {
		return ilosc;
	}
	public void setIlosc(int ilosc) {
		this.ilosc = ilosc;
	}
	public String getJednostka() {
		return jednostka;
	}
	public void setJednostka(String jednostka) {
		this.jednostka = jednostka;
	}
	
	public void dostawa(int dostawa){
		ilosc+=dostawa;
	}
	
	public void kupno(int kupno){
		if (ilosc>= kupno){
		ilosc-=kupno;
		}
	}
	
	public void infoProdukt(){
		System.out.println(nazwa+" "+ilosc+" "+jednostka);
	}
	
	

}